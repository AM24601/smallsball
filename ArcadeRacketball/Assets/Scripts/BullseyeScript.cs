﻿using UnityEngine;
using System.Collections;

public class BullseyeScript : Element
{
	void Start () 
    {
        GetComponent<Renderer>().material.SetFloat("_INNER_CIRCLE", app.model.goalInnerCircle);
        GetComponent<Renderer>().material.SetFloat("_OUTER_CIRCLE", app.model.goalOuterCircle);
	}
}
