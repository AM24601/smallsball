﻿using UnityEngine;
using System.Collections;

public class BarrierController : Element
{
    public void reset()
    {
        app.view.barrier.getAsset().GetComponent<BoxCollider>().enabled = true;
        app.view.barrier2.getAsset().GetComponent<CapsuleCollider>().enabled = true;
    }

    public void removeBarrier()
    {
        app.view.barrier.getAsset().GetComponent<BoxCollider>().enabled = false;
        app.view.barrier2.getAsset().GetComponent<CapsuleCollider>().enabled = false;
    }
}
