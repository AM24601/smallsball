﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Controller : Element
{
    public PlayerController[] players;
    public BallController ball;
    public HUDController HUD;
    public Dictionary<TeamKey, TeamController> teams;
    public Dictionary<TeamKey, GoalController> goals;
    public BarrierController barrier;

	// Use this for initialization
	void Start ()
    {
        teams = new Dictionary<TeamKey, TeamController>();
        goals = new Dictionary<TeamKey, GoalController>();

        players = new PlayerController[app.numPlayers];

        for (int i = 0; i < app.numPlayers; ++i)
        {
            players[i] = gameObject.transform.FindChild("Player" + (i + 1)).GetComponent<PlayerController>();
            players[i].setID(i);
        }

        ball = gameObject.transform.FindChild("Ball").GetComponent<BallController>();

        teams.Add(TeamKey.RED, gameObject.transform.FindChild("redTeamController").GetComponent<TeamController>());
        teams.Add(TeamKey.BLUE, gameObject.transform.FindChild("blueTeamController").GetComponent<TeamController>());

        teams[TeamKey.RED].teamKey = TeamKey.RED;
        teams[TeamKey.BLUE].teamKey = TeamKey.BLUE;

        goals.Add(TeamKey.RED, gameObject.transform.FindChild("RedGoalController").GetComponent<GoalController>());
        goals.Add(TeamKey.BLUE, gameObject.transform.FindChild("BlueGoalController").GetComponent<GoalController>());

        goals[TeamKey.RED].SetKey(TeamKey.BLUE);
        goals[TeamKey.BLUE].SetKey(TeamKey.RED);

        HUD = gameObject.transform.FindChild("HUDController").GetComponent<HUDController>();
        barrier = gameObject.transform.FindChild("Barrier").GetComponent<BarrierController>();
	}

    void Update()
    {
        if (app.model.startingPhase)
        {
            app.model.timer -= Time.deltaTime;
            if(app.model.timer > 2 && app.model.timer <= 3.9)
            {
                app.view.countdownThree.SetActive(true);
            }
            if(app.model.timer > 1 && app.model.timer <= 2.9)
            {
                app.view.countdownThree.SetActive(false);
                app.view.countdownTwo.SetActive(true);
            }
            if(app.model.timer > 0 && app.model.timer <= 1.9)
            {
                app.view.countdownTwo.SetActive(false);
                app.view.countdownOne.SetActive(true);
            }

            if (app.model.timer <= 0)
            {
                app.view.countdownOne.SetActive(false);
                app.model.startingPhase = false;
                beginPlay();
                app.model.timer = app.model.beginningTime;
                app.model.gamePhase = true;
            }
        }
        if (app.model.gamePhase)
        {
            app.model.countdownTimer -= Time.deltaTime;

            if(app.model.countdownTimer <= 0)
            {
                app.model.gamePhase = false;
                app.model.countdownTimer = app.model.countdownTime;
                app.endGame();
            }
        }
        if(app.model.goalShown)
        {
            app.model.goalTimer -= Time.deltaTime;
            app.view.goalScored.SetActive(true);
            if (app.model.goalTimer <= 0)
            {
                app.view.goalScored.SetActive(false);
                app.controller.reset();
                app.model.goalTimer = app.model.goalTime;
                app.model.goalShown = false;
            }
        }
    }

    public void beginPlay()
    {
        barrier.removeBarrier();
        ball.wake();
        countdown();
    }

    public void reset()
    {
        app.model.startingPhase = true;

        ball.reset();
        barrier.reset();

        for (int i = 0; i < app.numPlayers; ++i)
        {
            players[i].reset();
        }
    }
    public void countdown()
    {
        
    }
}
