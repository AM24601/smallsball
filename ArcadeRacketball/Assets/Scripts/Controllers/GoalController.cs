﻿using UnityEngine;
using System.Collections;

public class GoalController : Element
{
    public TeamKey teamKey;
    public TargetController target;
    
    void Awake()
    {
        target = gameObject.transform.FindChild("Target").GetComponent<TargetController>();
    }

    public void SetKey(TeamKey key)
    {
        teamKey = key;
        target.teamKey = key;
    }

    public void OnBallEnter()
    {
         if (!app.view.goals[teamKey].goalParticles.isPlaying) app.view.goals[teamKey].goalParticles.Play();
    }

    public void OnPointScore(Collider other)
    {
        int points;

        app.view.goals[teamKey].getAsset().GetComponent<AudioSource>().Play();
        app.view.goals[teamKey].goalParticles.GetComponent<AudioSource>().Play();


        Transform targetTransform = app.view.goals[teamKey].getAsset().transform.FindChild("Goal_center").transform;
        Vector3 targetCenter = new Vector3(targetTransform.position.x, targetTransform.position.y, targetTransform.position.z);
        Vector3 ballPosition = new Vector3(other.transform.position.x, other.transform.position.y, targetTransform.position.z);

        Vector2 ball = new Vector2(ballPosition.x, ballPosition.y);
        Vector2 center = new Vector2(targetCenter.x, targetCenter.y);

        float pointInGoal = Vector2.Distance(ball, center);

        if (pointInGoal < app.model.goalInnerCircle)
        {
            points = 5;
        }
        else if (pointInGoal < app.model.goalOuterCircle)
        {
            points = 3;
        }
        else
        {
            points = 1;
        }

        app.controller.teams[teamKey].addScore(points);
    }
}
