﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDController : Element
{
    void Update()
    {
        if (app.model.startingPhase)
        {
            app.view.HUD.transform.FindChild("Time").GetComponent<Text>().text = "";
        }
        else
        {
            app.view.HUD.transform.FindChild("Time").GetComponent<Text>().text = app.model.countdownTimer.ToString("F0");
        }
        app.view.HUD.transform.FindChild("RedScore").GetComponent<Text>().text = string.Format("Red Team: {0}", app.model.teams[TeamKey.RED].score);
        app.view.HUD.transform.FindChild("BlueScore").GetComponent<Text>().text = string.Format("Blue Team: {0}", app.model.teams[TeamKey.BLUE].score);

    }
}
