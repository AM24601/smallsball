﻿using UnityEngine;
using XInputDotNetPure;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : Element 
{
    public int playerID;
    public TrackerController tracker;

    void Awake()
    {
        tracker = gameObject.transform.FindChild("BallTracker").GetComponent<TrackerController>();
    }

    public void setID(int id)
    {
        playerID = id;
        tracker.playerID = id;
    }

    void updateMovement()
    {
        PlayerModel playerModel = app.model.players[playerID];

        //base movement
        playerModel.moveDirection = new Vector3(playerModel.state.ThumbSticks.Left.X * 25.0f * Time.deltaTime, playerModel.moveDirection.y, playerModel.state.ThumbSticks.Left.Y * 25.0f * Time.deltaTime);

        //setting gravity movement to zero if player is on ground
        if (app.view.players[playerID].characterController.isGrounded)
        {
            playerModel.moveDirection.y = 0;
        }
        
        playerModel.moveDirection = app.view.players[playerID].getAsset().transform.TransformDirection(playerModel.moveDirection);

        //baseing movement based on how long swing is
        if (playerModel.timeHeld > 1)
        {
            if (app.view.players[playerID].characterController.isGrounded)
            {
                playerModel.moveDirection.y *= (playerModel.speed / playerModel.timeHeld);
            }

            playerModel.moveDirection.x *= (playerModel.speed / playerModel.timeHeld);
            playerModel.moveDirection.z *= (playerModel.speed / playerModel.timeHeld);
        }
        else
        {
            if (app.view.players[playerID].characterController.isGrounded)
            {
                playerModel.moveDirection.y *= playerModel.speed;
            }

            playerModel.moveDirection.x *= playerModel.speed;
            playerModel.moveDirection.z *= playerModel.speed;
        }


        //jumping with charged jump
        if (app.view.players[playerID].characterController.isGrounded && playerModel.state.Buttons.A == ButtonState.Pressed)
        {
            playerModel.heldJump += Time.deltaTime;
        }
        else if (app.view.players[playerID].characterController.isGrounded && playerModel.prevState.Buttons.A == ButtonState.Pressed)
        {
            jump();
            //app.model.startVibe = false;
        }

        bool canBunt = app.view.players[playerID].anim.GetCurrentAnimatorStateInfo(1).IsName("Idle_Animation") && !app.view.players[playerID].anim.GetNextAnimatorStateInfo(1).IsName("Bunt");

        //lob ball up
        if (playerModel.prevState.Buttons.RightShoulder == ButtonState.Pressed && canBunt)
        {
            app.view.players[playerID].anim.SetTrigger(app.model.buntHash);
            if (isBallInRange())
            {
                app.view.ball.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(1.0f, 75.0f, 1.0f));
            }
        }

        //swinging
        if (playerModel.state.Triggers.Right != 0)
        {
            if (!app.view.players[playerID].anim.GetCurrentAnimatorStateInfo(1).IsName("Swing_Animation"))
            {
                playerModel.timeHeld += Time.deltaTime;
            }
        }

        if (playerModel.state.Triggers.Right == 0 && playerModel.timeHeld > 0)
        {
            swing();
            playerModel.timeHeld = 0;
            //app.model.startVibe = false;
        }

        //rumble effect
        if(playerModel.timeHeld >= playerModel.maxHitChargeTime || playerModel.heldJump >= playerModel.jumpThreshold)
        {
            app.model.startVibe = true;
        }
        else
        {
            app.model.startVibe = false;
        }

        //boost moving
        if (app.view.players[playerID].characterController.isGrounded && playerModel.state.Buttons.LeftStick == ButtonState.Pressed && playerModel.boostReady)
        {
            playerModel.boostReady = false;
            playerModel.boostStart = true;
        }
        if (app.view.players[playerID].characterController.isGrounded && playerModel.state.Buttons.LeftStick == ButtonState.Released)
        {
            playerModel.boostReady = true;
            playerModel.boostStart = false;
        }

        //apply movement and gravity to player
        playerModel.moveDirection.y -= playerModel.gravity * Time.deltaTime;
        app.view.players[playerID].characterController.Move(playerModel.moveDirection * Time.deltaTime);
    }

    void jump()
    {
        PlayerModel playerModel = app.model.players[playerID];

        if (playerModel.heldJump > 0 && playerModel.heldJump < playerModel.jumpThreshold)
        {
            playerModel.moveDirection.y = playerModel.jumpSpeed + (playerModel.heldJump * playerModel.jumpSpeed);
        }
        else
        {
            playerModel.moveDirection.y = playerModel.chargeJump;
        }

        playerModel.heldJump = 0;
    }

    bool isBallInRange()
    {
        return app.view.players[playerID].getAsset().transform.FindChild("HitCube").gameObject.GetComponent<BoxCollider>().bounds.Intersects(app.view.ball.gameObject.GetComponent<SphereCollider>().bounds);
    } 

    void swing()
    {
        if (app.view.players[playerID].anim.GetNextAnimatorStateInfo(1).IsName("Swing_Back_Animation") ||
            app.view.players[playerID].anim.GetNextAnimatorStateInfo(1).IsName("Charge_Shake") ||
            (app.view.players[playerID].anim.GetCurrentAnimatorStateInfo(1).IsName("Swing_Back_Animation") &&
            !app.view.players[playerID].anim.GetNextAnimatorStateInfo(1).IsName("Swing_Animation")) ||
            (app.view.players[playerID].anim.GetCurrentAnimatorStateInfo(1).IsName("Charge_Shake") &&
            !app.view.players[playerID].anim.GetNextAnimatorStateInfo(1).IsName("Swing_Animation")))
        {
            if (isBallInRange())
            {
                PlayerModel playerModel = app.model.players[playerID];
                Vector3 camDir = app.view.players[playerID].getAsset().transform.Find("CameraBoom").gameObject.transform.forward;

                camDir.Normalize();

                app.model.players[playerID].movement = camDir * app.model.players[playerID].force * Mathf.Clamp(app.model.players[playerID].timeHeld, 1, playerModel.maxHitChargeTime);

                app.view.ball.gameObject.GetComponent<Rigidbody>().AddForce(app.model.players[playerID].movement);
                app.view.players[playerID].getAsset().GetComponent<AudioSource>().Play();

                if (playerModel.state.Triggers.Right == 0 && playerModel.timeHeld >= playerModel.maxHitChargeTime && isBallInRange())
                {
                    app.view.players[playerID].chargeHit.Play();
                }
            }

            app.view.players[playerID].anim.SetTrigger(app.model.swingHash);
        }
    }

    void updateVibeTime()
    {
        if(app.model.startVibe)
        {
            GamePad.SetVibration(app.model.players[playerID].mPlayerIndex, 0.5f, 0.5f);
        }
        else
        {
            GamePad.SetVibration(app.model.players[playerID].mPlayerIndex, 0.0f, 0.0f);
        }
    }

    void updateCamera()
    {
        float xDiff =  app.model.players[playerID].state.ThumbSticks.Right.X;
        float yDiff =  app.model.players[playerID].state.ThumbSticks.Right.Y;
        Vector2 mouseDiff = new Vector2(xDiff, yDiff);

        if (app.view.players[playerID].camera.transform.localRotation.x <= -0.25F && yDiff > 0)
        {
            app.view.players[playerID].camera.moveCamera(new Vector2(xDiff, 0.0f));
            /*if (app.view.players[playerID].camera.transform.localPosition.z <= 3.5F && app.view.players[playerID].camera.transform.localPosition.y <= 3.5F)
            {
                app.view.players[playerID].camera.transform.localPosition = new Vector3(app.view.players[playerID].camera.transform.localPosition.x, app.view.players[playerID].camera.transform.localPosition.y + yDiff, app.view.players[playerID].camera.transform.localPosition.z + yDiff);
                app.view.players[playerID].camera.transform.localRotation = new Quaternion(app.view.players[playerID].camera.transform.localRotation.x - yDiff, app.view.players[playerID].camera.transform.localRotation.y, app.view.players[playerID].camera.transform.localRotation.z, 1);
                app.view.players[playerID].camera.moveCamera(mouseDiff);
            }
            else
            {
                app.view.players[playerID].camera.moveCamera(new Vector2(xDiff,0.0f));
            }*/
        }
        /*else if(app.view.players[playerID].camera.transform.localPosition.z >= app.view.players[playerID].startZ && app.view.players[playerID].camera.transform.localPosition.y >= app.view.players[playerID].startY && yDiff < 0)
        {
            app.view.players[playerID].camera.transform.localPosition = new Vector3(app.view.players[playerID].camera.transform.localPosition.x, app.view.players[playerID].camera.transform.localPosition.y + yDiff, app.view.players[playerID].camera.transform.localPosition.z + yDiff);
        }*/
        else
        {
            app.view.players[playerID].camera.moveCamera(mouseDiff);
        }

        if (isBallInRange() && app.view.players[playerID].reticle.texture != app.view.reticleTextures[app.view.reticleTextures.Length - 1])
        {
            app.view.players[playerID].reticle.texture = app.view.reticleTextures[app.view.reticleTextures.Length - 1];
        }
        else if (!isBallInRange() && app.view.players[playerID].reticle.texture != app.view.reticleTextures[0])
        {
             app.view.players[playerID].reticle.texture = app.view.reticleTextures[0];
        }
    }

    void updateAnimation()
    {
        Vector2 landSpeed = new Vector2(app.model.players[playerID].moveDirection.x, app.model.players[playerID].moveDirection.z);

        app.view.players[playerID].anim.SetFloat(app.model.speedHash, landSpeed.magnitude);
        app.view.players[playerID].anim.SetBool(app.model.airbornHash, !app.view.players[playerID].characterController.isGrounded);

        if (!app.view.players[playerID].anim.GetCurrentAnimatorStateInfo(1).IsName("Idle_Animation"))
        {
            app.view.players[playerID].anim.SetLayerWeight(1, 1.0f);
            app.view.players[playerID].anim.SetLayerWeight(2, 0.0f);
        }
        else
        {
            app.view.players[playerID].anim.SetLayerWeight(1, 0.0f);
            app.view.players[playerID].anim.SetLayerWeight(2, 1.0f);
        }

        if (app.view.players[playerID].anim.GetCurrentAnimatorStateInfo(1).IsName("Idle_Animation"))
        {
            app.view.players[playerID].anim.SetFloat(app.model.chargeHash, app.model.players[playerID].timeHeld);
        }
    }

    void updateBoost()
    {
        PlayerModel playerModel = app.model.players[playerID];
        if(playerModel.boostTime < 0)
        {
            playerModel.boostCooldown -= Time.deltaTime;    
        }
        if(playerModel.boostStart)
        {
            playerModel.boostTime -= Time.deltaTime;
            if(playerModel.boostTime > 0)
            {
                app.view.players[playerID].trail.enabled = true;
                app.view.players[playerID].anim.SetBool(app.model.runHash, true);
                playerModel.speed = playerModel.boostSpeed;
            }
            else
            {
                app.view.players[playerID].anim.SetBool(app.model.runHash, false);
            }
        }
        if(playerModel.boostCooldown < 0)
        {
            playerModel.boostReady = true;
            playerModel.boostTime = 2;
            playerModel.boostCooldown = 5;
        }
        if(playerModel.boostTime < 0 || !playerModel.boostStart)
        {
            playerModel.boostStart = false;
            app.view.players[playerID].trail.enabled = false;
            app.view.players[playerID].anim.SetBool(app.model.runHash, false);
            playerModel.speed = playerModel.startSpeed;
        }
    }

    void Update()
    {
        app.model.players[playerID].prevState = app.model.players[playerID].state;
        app.model.players[playerID].state = GamePad.GetState(app.model.players[playerID].mPlayerIndex);
        if (!app.model.startingPhase || !app.model.players[playerID].isForward)
        {

            updateParticles();
            updateMovement();
            updateCamera();
            updateVibeTime();
            updateBoost();
            updateAnimation();
        }
    }

    void updateParticles()
    {
        PlayerModel playerModel = app.model.players[playerID];

        //JUMP PARTICLE
        if (!app.view.players[playerID].jumpEmitter.isPlaying && playerModel.heldJump > 0)
        {
            app.view.players[playerID].jumpEmitter.startColor = app.model.jumpChargingColor;
            app.view.players[playerID].jumpEmitter.Play();
        }

        if (app.view.players[playerID].jumpEmitter.isPlaying)
        {
            

            if (app.view.players[playerID].characterController.isGrounded)
            {
                if (playerModel.heldJump >= playerModel.jumpThreshold)
                {
                    app.view.players[playerID].jumpEmitter.startColor = app.model.jumpChargedColor;
                }
                else if (playerModel.heldJump <= 0)
                {
                    app.view.players[playerID].jumpEmitter.Stop();
                }
            }
            else if (playerModel.moveDirection.y < 0)
            {
                app.view.players[playerID].jumpEmitter.Stop();
            }
        }

        //SWING PARTICLE
        if (!app.view.players[playerID].swingEmitter.isPlaying && playerModel.timeHeld > 0)
        {
            app.view.players[playerID].swingEmitter.startColor = app.model.swingChargingColor;
            app.view.players[playerID].swingEmitter.Play();
        }

        if (app.view.players[playerID].swingEmitter.isPlaying)
        {
            if (playerModel.timeHeld >= playerModel.maxHitChargeTime)
            {
                app.view.players[playerID].swingEmitter.startColor = app.model.swingChargedColor;
            }
            else if (playerModel.timeHeld <= 0)
            {
                app.view.players[playerID].swingEmitter.Stop();
            }
        }

        //CHARGE SWING PARTICLE
        if (playerModel.state.Triggers.Right == 0 && playerModel.timeHeld >= playerModel.maxHitChargeTime && isBallInRange())
        {
            app.view.players[playerID].hitEmitter.Play();
            app.view.players[playerID].chargeHit.Play();
        }

    }

    public void reset()
    {
        app.view.players[playerID].camera.reset();
        app.view.players[playerID].getAsset().transform.position = app.model.players[playerID].mStartPosition;

        app.view.players[playerID].anim.SetFloat(app.model.speedHash, 0);
        app.view.players[playerID].anim.SetBool(app.model.airbornHash, false);

        app.view.players[playerID].anim.SetFloat(app.model.chargeHash, 0);

        app.model.players[playerID].boostReady = true;
        app.model.players[playerID].boostStart = false;
        app.model.players[playerID].boostTime = 2;
        app.model.players[playerID].boostCooldown = 5;
    }
}