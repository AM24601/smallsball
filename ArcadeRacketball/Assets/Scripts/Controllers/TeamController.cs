﻿using UnityEngine;
using System.Collections;

public class TeamController : Element
{
    public TeamKey teamKey;

    public void addScore(int score)
    {
        app.model.teams[teamKey].score += score;
    }

    public int getScore()
    {
        return app.model.teams[teamKey].score;
    }
}
