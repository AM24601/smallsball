﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndGame : Element {

    public GameObject endUI;
    public GameObject ball;
    public Camera endScreenCam;

    void Update()
    {
        endUI.transform.FindChild("Text").GetComponent<Text>().text = string.Format("Red Team: {0}   |   Blue Team: {1}", app.model.teams[TeamKey.RED].score, app.model.teams[TeamKey.BLUE].score);
        if (app.model.teams[TeamKey.RED].score > app.model.teams[TeamKey.BLUE].score)
        {
            endUI.transform.FindChild("GameOver").GetComponent<Text>().text = string.Format("Red Team Wins");
        }
        else if (app.model.teams[TeamKey.RED].score < app.model.teams[TeamKey.BLUE].score)
        {
            endUI.transform.FindChild("GameOver").GetComponent<Text>().text = string.Format("Blue Team Wins");
        }
        else
        {
            endUI.transform.FindChild("GameOver").GetComponent<Text>().text = string.Format("Tie!!!");
        }

        if (app.model.players[0].state.Buttons.A == XInputDotNetPure.ButtonState.Pressed)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("mainmenu");
        }

        endScreenCam.transform.LookAt(ball.transform.position);

        Debug.Log(endScreenCam.transform.position.x);
        Debug.Log(endScreenCam.transform.position.y);
        Debug.Log(endScreenCam.transform.position.z);

        if (endScreenCam.transform.position.x > 23 && endScreenCam.transform.position.z < 50)
        {
            endScreenCam.transform.position = new Vector3(endScreenCam.transform.position.x, endScreenCam.transform.position.y, endScreenCam.transform.position.z + 0.25f);
        }
        if (endScreenCam.transform.position.x > -30 && endScreenCam.transform.position.z > 50)
        {
            endScreenCam.transform.position = new Vector3(endScreenCam.transform.position.x - 0.25f, endScreenCam.transform.position.y, endScreenCam.transform.position.z);
        }
        if (endScreenCam.transform.position.x < -30 && endScreenCam.transform.position.z > -54)
        {
            endScreenCam.transform.position = new Vector3(endScreenCam.transform.position.x, endScreenCam.transform.position.y, endScreenCam.transform.position.z - 0.25f);
        }
        if (endScreenCam.transform.position.x < 23 && endScreenCam.transform.position.z < -54)
        {
            endScreenCam.transform.position = new Vector3(endScreenCam.transform.position.x + 0.25f, endScreenCam.transform.position.y, endScreenCam.transform.position.z);
        }

    }
}
