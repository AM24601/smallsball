﻿using UnityEngine;
using System.Collections;

public class GoalModel : Element 
{
    public TargetModel target;

    void Start()
    {
        target = gameObject.transform.FindChild("Target").GetComponent<TargetModel>();
    }
}
