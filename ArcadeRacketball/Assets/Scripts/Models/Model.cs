﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Model : Element
{
    public PlayerModel[] players;
    public BallModel ball;
    public Dictionary<TeamKey, TeamModel> teams;
    public Dictionary<TeamKey, GoalModel> goals;
    public bool startingPhase;
    public float beginningTime = 5.0f;
    public float timer;

    public float goalTimer;
    public float goalTime = 3.0f;
    public bool goalShown = false;

    public float vibeTime = 0.0f;
    public bool startVibe = false;

    public bool gamePhase;
    public float countdownTime = 30.0F;
    public float countdownTimer;

    public float goalInnerCircle;
    public float goalOuterCircle;

    public Color jumpChargingColor;
    public Color jumpChargedColor;

    public Color swingChargingColor;
    public Color swingChargedColor;

    public int speedHash = Animator.StringToHash("Speed");
    public int airbornHash = Animator.StringToHash("Airborn");
    public int chargeHash = Animator.StringToHash("Charge");
    public int swingHash = Animator.StringToHash("Swing");
    public int runHash = Animator.StringToHash("Running");
    public int buntHash = Animator.StringToHash("Bunt");

    // Use this for initialization
    void Awake ()
    {
        goalInnerCircle = 1;
        goalOuterCircle = 3;
        startingPhase = true;
        gamePhase = false;
        teams = new Dictionary<TeamKey, TeamModel>();
        goals = new Dictionary<TeamKey, GoalModel>();

        players = new PlayerModel[app.numPlayers];

        for (int i = 0; i < app.numPlayers; ++i)
        {
            players[i] = gameObject.transform.FindChild("Player" + (i + 1)).GetComponent<PlayerModel>();
        }

        ball = gameObject.transform.FindChild("Ball").GetComponent<BallModel>();

        teams.Add(TeamKey.RED, gameObject.transform.FindChild("redTeamModel").GetComponent<TeamModel>());
        teams.Add(TeamKey.BLUE, gameObject.transform.FindChild("blueTeamModel").GetComponent<TeamModel>());

        goals.Add(TeamKey.RED, gameObject.transform.FindChild("RedGoalModel").GetComponent<GoalModel>());
        goals.Add(TeamKey.BLUE, gameObject.transform.FindChild("BlueGoalModel").GetComponent<GoalModel>());

        timer = beginningTime;
        countdownTimer = countdownTime;
        goalTimer = goalTime;
	}
}
