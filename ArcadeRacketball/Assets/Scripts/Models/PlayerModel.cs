﻿using UnityEngine;
using XInputDotNetPure;
using System.Collections;

public class PlayerModel : Element
{
    public float speed = 20.0F;
    public float startSpeed = 20.0F;
    public float boostSpeed = 40.0F;
    public float jumpSpeed = 8.0F;
    public float chargeJump = 16.0F;
    public float gravity = 20.0F;
    public int playerNumber;
    public Vector3 movement = Vector3.zero;
    public float jumpThreshold = 10;
    public float maxHitChargeTime = 3;

    public GamePadState state;
    public GamePadState prevState;
    public PlayerIndex mPlayerIndex;

    public float force = 500;
    public float timeHeld = 0;
    public float heldJump = 0;
    public float boostCooldown = 5;
    public float boostTime = 3;
    public bool startSwing = false;
    public bool isForward;

    public bool boostReady = true;
    public bool boostStart = false;

    public Vector3 moveDirection = Vector3.zero;

    public Vector3 mStartPosition = Vector3.zero;
    public Quaternion mStartRotation;

    // Use this for initialization
    void Start ()
    {
        mPlayerIndex = (PlayerIndex)(playerNumber - 1);
        mStartRotation = Quaternion.Euler(0, 0, 0);

        state = GamePad.GetState(mPlayerIndex);
	}
}
