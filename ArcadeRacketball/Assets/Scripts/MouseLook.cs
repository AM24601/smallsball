﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Camera/Simple Smooth Mouse Look ")]
public class MouseLook : MonoBehaviour
{
    Vector2 mRotation;
    Vector2 smoothed;

    public Vector2 clampInDegrees = new Vector2(360, 180);
    public bool lockCursor;
    public Vector2 sensitivity = new Vector2(2, 2);
    public Vector2 smoothing = new Vector2(3, 3);
    public Vector2 targetDirection;
    public Vector2 targetCharacterDirection;

    // Assign this if there's a parent object controlling motion, such as a Character Controller.
    // Yaw rotation will affect this object instead of the camera if set.
    public GameObject characterBody;
    public int        playerNumber;
    public Vector2      delta;

    public bool end;

    void Start()
    {
        end = true;
        // Set target direction to the camera's initial orientation.
        targetDirection = transform.localRotation.eulerAngles;
        // Set target direction for the character body to its inital state.
        if (characterBody) targetCharacterDirection = characterBody.transform.localRotation.eulerAngles;
    }

    public void moveCamera(Vector2 d)
    {
        delta = d;
    }

    public void reset()
    {
        mRotation = Vector2.zero;
    }

    void Update()
    {
        // Scale input against the sensitivity setting and multiply that against the smoothing value.
        delta = Vector2.Scale(delta, new Vector2(sensitivity.x * smoothing.x, sensitivity.y * smoothing.y));

        // Interpolate mouse movement over time to apply smoothing delta.
        smoothed.x = Mathf.Lerp(smoothed.x, delta.x, 1f / smoothing.x);
        smoothed.y = Mathf.Lerp(smoothed.y, delta.y, 1f / smoothing.y);

        // Find the absolute mouse movement value from point zero.
        mRotation += smoothed;

        // Clamp and apply the local x value first, so as not to be affected by world transforms.
        if (clampInDegrees.x < 360)
            mRotation.x = Mathf.Clamp(mRotation.x, -clampInDegrees.x * 0.5f, clampInDegrees.x * 0.5f);

        var xRotation = Quaternion.AngleAxis(-mRotation.y, Vector3.right);
        transform.localRotation = xRotation;

        // Then clamp and apply the global y value.
        if (clampInDegrees.y < 360)
            mRotation.y = Mathf.Clamp(mRotation.y, -clampInDegrees.y * 0.5f, clampInDegrees.y * 0.5f);

        // If there's a character body that acts as a parent to the camera
        if (characterBody)
        {
            var yRotation = Quaternion.AngleAxis(mRotation.x, characterBody.transform.up);
            characterBody.transform.localRotation = yRotation;
        }
        else
        {
            var yRotation = Quaternion.AngleAxis(mRotation.x, transform.InverseTransformDirection(Vector3.up));
            transform.localRotation *= yRotation;
        }

        delta = Vector2.zero;
    }
}
