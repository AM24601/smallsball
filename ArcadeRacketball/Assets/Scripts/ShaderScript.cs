﻿using UnityEngine;
using System.Collections;

public class ShaderScript : Element
{
    bool hit;
    float time;

	// Use this for initialization 
	void Start ()
    {
        hit = false;
        time = 0.0f;
        GetComponent<Renderer>().material.SetVector("_SCALE", transform.localScale);
	}

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject == app.view.ball.gameObject && other.contacts.Length > 0)
        {

            Vector3 relativePos = other.contacts[0].point -transform.position;

            Vector3 norm = Vector3.Normalize(other.contacts[0].normal);

            GetComponent<Renderer>().material.SetVector("_POI", relativePos);
            GetComponent<Renderer>().material.SetVector("_NORMAL", norm);

            if (other.contacts[0].point.z > 0)
            {
                GetComponent<Renderer>().material.SetColor("_COLOR", Color.red);
            }
            else if (other.contacts[0].point.z < 0)
            {
                GetComponent<Renderer>().material.SetColor("_COLOR", Color.blue);
            }

            hit = true;
        }
    }

    void resetShader()
    {
        hit = false;
        time = 0.0f;
        GetComponent<Renderer>().material.SetFloat("_TIME", 0.0f);
        GetComponent<Renderer>().material.SetFloat("_RADIUS", 0.0f);
    }

    void Update()
    {
        if (hit)
        {
            time += Time.deltaTime;
            GetComponent<Renderer>().material.SetFloat("_TIME", time);

            if (time >= GetComponent<Renderer>().material.GetFloat("_END_TIME"))
            {
                resetShader();
            }
        }
    }
}
