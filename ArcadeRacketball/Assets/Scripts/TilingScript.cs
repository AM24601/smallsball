﻿using UnityEngine;
using System.Collections;

public class TilingScript : MonoBehaviour {

    public Renderer rend;

    // Use this for initialization
    void Start()
    {

        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        float Xscale = 7f;
        float Yscale = 7f;
        rend.material.mainTextureScale = new Vector2(Xscale, Yscale);

    }
}
