﻿using UnityEngine;
using System.Collections;

public class BallView : Element
{

    public AudioSource sound;

    void Start()
    {
        gameObject.transform.GetComponent<Rigidbody>().Sleep();
        sound = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        app.controller.ball.ReflectBall(collisionInfo);
        if(collisionInfo.gameObject.name == "Wall1" || collisionInfo.gameObject.name == "Wall2" || collisionInfo.gameObject.name == "Wall3" || collisionInfo.gameObject.name == "Wall4")
        {
            sound.Play();
        }
    }

    public GameObject GetAsset()
    {
        return gameObject;
    }
}
