﻿using UnityEngine;
using System.Collections;

public class GoalView : Element 
{
    public TeamKey teamKey;
    public TargetView target;
    public ParticleSystem goalParticles;

    void Awake()
    {
        target = getAsset().transform.FindChild("Goal_center").GetComponent<TargetView>();
        goalParticles = getAsset().transform.FindChild("GoalParticle").GetComponent<ParticleSystem>();
    }

    public void SetKey(TeamKey key, TeamKey goalKey)
    {
        teamKey = key;
        target.teamKey = key;
        target.goalKey = goalKey;
    }

    public GameObject getAsset()
    {
        return gameObject.transform.FindChild("asset").gameObject;
    } 
}
