﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerView : Element
{
    public int playerID;
    public TrackerView tracker;
    public CharacterController characterController;
    public ParticleSystem jumpEmitter;
    public ParticleSystem swingEmitter;
    public ParticleSystem hitEmitter;
    public RawImage reticle;
    public MouseLook camera;
    public GameObject Animation;
    public TrailRenderer trail;
    public Animator anim;
    public AudioSource chargeHit;
    public float startZ;
    public float startY;

	// Use this for initialization
	void Awake ()
    {
        tracker = getAsset().transform.FindChild("BallTracker").GetComponent<TrackerView>();
        characterController = getAsset().GetComponent<CharacterController>();
        jumpEmitter = getAsset().transform.FindChild("jumpParticle").GetComponent<ParticleSystem>();
        swingEmitter = getAsset().transform.FindChild("swingParticle").GetComponent<ParticleSystem>();

        hitEmitter = getAsset().transform.FindChild("Ring").GetComponent<ParticleSystem>();
        hitEmitter.Stop();

        chargeHit = getAsset().transform.FindChild("Ring").GetComponent<AudioSource>();

        reticle = getAsset().transform.FindChild("CameraBoom").FindChild("Camera").FindChild("HUD").FindChild("Reticle").GetComponent<RawImage>();
        camera = getAsset().transform.Find("CameraBoom").gameObject.GetComponent<MouseLook>();
        Animation = getAsset().transform.FindChild("Animation").FindChild("LocalAnimation").gameObject;
        anim = Animation.GetComponent<Animator>();
        trail = getAsset().transform.FindChild("TrailRenderer").GetComponent<TrailRenderer>();
        trail.enabled = false;
	}

    void Start()
    {
        startZ = camera.transform.localPosition.z;
        startY = camera.transform.localPosition.y;
    }

    public GameObject getAsset()
    {
        return gameObject.transform.FindChild("asset").gameObject;
    }
}
