﻿using UnityEngine;
using System.Collections;

public class TargetView : Element
{
    public TeamKey teamKey;
    public TeamKey goalKey;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == app.view.ball.gameObject && !app.model.goalShown)
        {
            app.model.goalShown = true;
            app.controller.goals[goalKey].OnBallEnter();
            app.model.goals[teamKey].target.hit = true;
            app.controller.goals[teamKey].OnPointScore(other);
        }
    }
}
