﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class View : Element
{
    public PlayerView[] players;
    public BallView ball;
    public Dictionary<TeamKey, GoalView> goals;
    public GameObject HUD;
    public GameObject countdownOne;
    public GameObject countdownTwo;
    public GameObject countdownThree;
    public GameObject goalScored;
    public BarrierView barrier;
    public BarrierView barrier2;
    public Texture[] reticleTextures;

	// Use this for initialization
	void Start ()
    {
        goals = new Dictionary<TeamKey, GoalView>();

        players = new PlayerView[app.numPlayers];

        for (int i = 0; i < app.numPlayers; ++i)
        {
            players[i] = gameObject.transform.FindChild("Player" + (i + 1)).GetComponent<PlayerView>();
            players[i].playerID = i;
        }

        ball = gameObject.transform.FindChild("Ball").GetComponent<BallView>();

        goals.Add(TeamKey.RED, gameObject.transform.FindChild("RedGoalView").GetComponent<GoalView>());
        goals.Add(TeamKey.BLUE, gameObject.transform.FindChild("BlueGoalView").GetComponent<GoalView>());

        goals[TeamKey.RED].SetKey(TeamKey.RED, TeamKey.BLUE);
        goals[TeamKey.BLUE].SetKey(TeamKey.BLUE, TeamKey.RED);

        HUD = gameObject.transform.FindChild("HUD").gameObject;
        countdownOne = gameObject.transform.FindChild("HUD").FindChild("One").gameObject;
        countdownTwo = gameObject.transform.FindChild("HUD").FindChild("Two").gameObject;
        countdownThree = gameObject.transform.FindChild("HUD").FindChild("Three").gameObject;
        goalScored = gameObject.transform.FindChild("HUD").FindChild("GOAL").gameObject;

        barrier = gameObject.transform.FindChild("Barrier").GetComponent<BarrierView>();
        barrier2 = gameObject.transform.FindChild("Barrier2").GetComponent<BarrierView>();
	}
}
