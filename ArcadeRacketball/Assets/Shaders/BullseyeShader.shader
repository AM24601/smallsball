﻿Shader "Hidden/NewImageEffectShader"
{
	Properties
	{
		_INNER_CIRCLE ("InnerCircle", Float) = 1.0
		_OUTER_CIRCLE ("OuterCircle", Float) = 2.0
		_InnerColor ("Inner Color", Color) = (1,1,1,1)
		_MiddleColor ("Middle Color", Color) = (1,1,1,1)
		_OuterColor ("Outer Color", Color) = (1,1,1,1)
		_Alpha ("Alpha", Range (0.0, 1.0)) = 1.0
	}
	SubShader
	{
		// No culling or depth
		//Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			float _INNER_CIRCLE;
			float _OUTER_CIRCLE;
			float4 _InnerColor;
			float4 _MiddleColor;
			float4 _OuterColor;
			float _Alpha;
	

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 localPos: TEXCOORD1;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.localPos = v.vertex;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				float d = distance(float2(0.0, 0.0), float2(i.localPos[0], i.localPos[1]));
				fixed4 col;
				if (d < _INNER_CIRCLE)
				{
					col = _InnerColor;
				}
				else if (d < _OUTER_CIRCLE)
				{
					col = _MiddleColor;
				}
				else
				{
					col = _OuterColor;
				}

				col.a = _Alpha;
				
				return col;
			}
			ENDCG
		}
	}
}
