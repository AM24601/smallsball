﻿Shader "Hidden/RippleShader"
{
	Properties
	{
		_POI("Impact Point", Vector) = (0.0, 0.0, 0.0)
		_RADIUS("Radius", float) = 0.0
		_RATE("Rate", float) = 2.0
		_TIME("Time", float) = 0.0
		_END_TIME("End Time", float) = 2.0
		_COLOR("Color", Color) = ( 0.0, 0.0, 0.0, 1.0)
		_MainTex ("Texture", 2D) = "white"
		_SCALE ("Scale", Vector) = (0.0, 0.0, 0.0)
	}
	SubShader
	{
		Tags{ /*"Queue" = "Transparent"*/ "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass
		{
			ZWrite Off
			ColorMask 0
		}

		Pass
		{
			ZWrite Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			float4 _POI;
			float _RATE;
			float _END_TIME;
			float _RADIUS;
			float _TIME;
			float4 _COLOR;
			float3 _SCALE;
			float4 _NORMAL;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 localPos: TEXCOORD1;
			};

			sampler2D _MainTex;

			v2f vert(appdata v)
			{
				v2f o;

				o.localPos = float4 (v.vertex[0] * _SCALE[0], v.vertex[1] * _SCALE[1], v.vertex[2] * _SCALE[2], v.vertex[3]);

				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;

				return o;
			}

			half4 frag(v2f i) : SV_Target
			{
				half4 col = tex2D(_MainTex, i.uv);

				float d = distance(_POI, i.localPos);
				_RADIUS = _RATE * _TIME; 
				float radius_2 = _RADIUS - 1.0f;
				float radius_3 = radius_2 - 1.0f;

				col.a = 0.0;

				float modifier1 = (max(0, _RADIUS) / _RADIUS) * max(0, 0.25 - (abs(d - _RADIUS)));
				float modifier2 = (max(0, radius_2) / radius_2) * max(0, 0.25 - (abs(d - radius_2)));
				float modifier3 = (max(0, radius_3) / radius_3) * max(0, 0.25 - (abs(d - radius_3)));

				float modifier = modifier1 + modifier2 + modifier3;

				col = col * _COLOR * modifier * 5.0f;
				col.a = _COLOR.a * modifier * 2.0f;


				return col;
			}
			ENDCG
		}
		
	}
}
