﻿using UnityEngine;
using System.Collections;

public enum TeamKey
{
    RED,
    BLUE
}

//Base class for all elements in application
public class Element : MonoBehaviour
{
    //Used to access the application from any children processes
    public Application app { get { return GameObject.FindObjectOfType<Application>();} }
}

public class Application : MonoBehaviour 
{
    //base model, view and controller scripts
    public int numPlayers = 4;
    public Model model;
    public View view;
    public Controller controller;

	// Use this for initialization
	void Start () 
    {
        model = gameObject.transform.FindChild("Model").GetComponent<Model>();
        view = gameObject.transform.FindChild("View").GetComponent<View>();
        controller = gameObject.transform.FindChild("Controller").GetComponent<Controller>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
