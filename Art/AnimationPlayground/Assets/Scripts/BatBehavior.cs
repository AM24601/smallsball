﻿using UnityEngine;
using System.Collections;

public class BatBehavior : MonoBehaviour 
{
    public float SwingSpeed;
    private Quaternion restingPos;
    private Quaternion fullSwingPos;
    private Quaternion backSwingPos;
    private bool isSwinging;
    private bool SwingingForward;


    // Use this for initialization
    void Start () 
    {
        isSwinging = false;
        SwingingForward = true;
        restingPos = transform.localRotation;
        fullSwingPos = transform.localRotation;
        fullSwingPos = Quaternion.Euler(0, -90, 0);
        backSwingPos = transform.localRotation;
        backSwingPos = Quaternion.Euler(0, 70, 0);
    }

    public void animate()
    {
        if (!isSwinging)
        {
            isSwinging = true;
            SwingingForward = true;
        }
    }

    public void swingBack()
    {
        transform.localRotation = Quaternion.Lerp(transform.localRotation, backSwingPos, Time.deltaTime * SwingSpeed);
    }

    // Update is called once per frame
    void Update () 
    {
        if (isSwinging)
        {
            if (SwingingForward)
            {
                transform.localRotation = Quaternion.Lerp(transform.localRotation, fullSwingPos, Time.deltaTime * SwingSpeed);
                if (transform.localRotation == fullSwingPos)
                {
                    SwingingForward = false;
                }
            }
            else
            {
                transform.localRotation = Quaternion.Lerp(transform.localRotation, restingPos, Time.deltaTime * SwingSpeed);
                if (transform.localRotation == restingPos)
                {
                    isSwinging = false;
                }
            }
        }
	}
}
