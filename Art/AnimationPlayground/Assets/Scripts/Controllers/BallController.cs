﻿using UnityEngine;
using System.Collections;

public class BallController : Element
{
    // Update is called once per frame
    void Update()
    {
        if (!app.model.startingPhase)
        {
            app.model.ball.movement = app.model.ball.rb.velocity;
        }
    }

    public void wake()
    {
        app.view.ball.GetAsset().transform.GetComponent<Rigidbody>().WakeUp();
        app.model.ball.rb.isKinematic = false;
    }

    public void sleep()
    {
        app.view.ball.GetAsset().transform.GetComponent<Rigidbody>().Sleep();
    }

    public void reset()
    {
        app.view.ball.GetAsset().transform.GetComponent<Rigidbody>().Sleep();
        app.view.ball.GetAsset().transform.position = app.model.ball.mInitialPos;
        app.model.ball.rb.velocity = Vector3.zero;
        app.model.ball.rb.isKinematic = true;
    }

    public void ReflectBall(Collision collisionInfo)
    {
        if (collisionInfo.contacts.Length > 0)
        {
             app.model.ball.rb.velocity = Vector3.Reflect(app.model.ball.movement, collisionInfo.contacts[0].normal) * app.model.ball.dampening;
        }
    }
}
