﻿using UnityEngine;
using System.Collections;

public class GoalController : Element
{
    public TeamKey teamKey;
    public TargetController target;
    
    void Awake()
    {
        target = gameObject.transform.FindChild("Target").GetComponent<TargetController>();
    }

    public void SetKey(TeamKey key)
    {
        teamKey = key;
        target.teamKey = key;
    }

    public void OnBallEnter()
    {
            if (!app.view.goals[teamKey].goalParticles.isPlaying) app.view.goals[teamKey].goalParticles.Play();
    }

    public void OnPointScore(Collider other)
    {
        int points;

        if (other.gameObject == app.view.ball.gameObject)
        {
            app.view.goals[teamKey].getAsset().GetComponent<AudioSource>().Play();


            Transform targetTransform = app.view.goals[teamKey].getAsset().transform.FindChild("Goal_center").transform;
            Vector3 targetCenter = new Vector3(targetTransform.position.x, targetTransform.position.y, targetTransform.position.z);
            Vector3 ballPosition = new Vector3(other.transform.position.x, other.transform.position.y, targetTransform.position.z);

            float pointInGoal = Mathf.Pow((ballPosition.x - targetCenter.x), 2) + Mathf.Pow((ballPosition.y - targetCenter.y), 2);

            if (pointInGoal < 1)
            {
                points = 5;
            }
            else if (pointInGoal < Mathf.Pow(2.0f, 2))
            {
                points = 3;
            }
            else
            {
                points = 1;
            }

            app.controller.teams[teamKey].addScore(points);

            app.controller.reset();
        }
    }
}
