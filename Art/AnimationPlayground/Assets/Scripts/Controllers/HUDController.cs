﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDController : Element
{
    void Update()
    {
        if (app.model.startingPhase)
        {
            app.view.HUD.transform.FindChild("Text").GetComponent<Text>().text = app.model.timer.ToString("F1");
        }
        else
        {
            app.view.HUD.transform.FindChild("Text").GetComponent<Text>().text = string.Format("Red Team: {0}   |   Blue Team: {1}", app.model.teams[TeamKey.RED].score, app.model.teams[TeamKey.BLUE].score);
        }

        app.view.HUD.transform.FindChild("Time").GetComponent<Text>().text = app.model.countdownTimer.ToString("F1");
    }
}
