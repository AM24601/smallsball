﻿using UnityEngine;
using XInputDotNetPure;
using System.Collections;

public class PlayerController : Element 
{
    public int playerID;
    public TrackerController tracker;

    void Awake()
    {
        tracker = gameObject.transform.FindChild("BallTracker").GetComponent<TrackerController>();
    }

    public void setID(int id)
    {
        playerID = id;
        tracker.playerID = id;
    }

    void updateMovement()
    {
        PlayerModel playerModel = app.model.players[playerID];

        playerModel.moveDirection = new Vector3(playerModel.state.ThumbSticks.Left.X * 25.0f * Time.deltaTime, playerModel.moveDirection.y, playerModel.state.ThumbSticks.Left.Y * 25.0f * Time.deltaTime);

        if (app.view.players[playerID].characterController.isGrounded)
        {
            playerModel.moveDirection.y = 0;
        }
        
        playerModel.moveDirection = app.view.players[playerID].getAsset().transform.TransformDirection(playerModel.moveDirection);

        if (playerModel.timeHeld > 1)
        {
            if (app.view.players[playerID].characterController.isGrounded)
            {
                playerModel.moveDirection.y *= (playerModel.speed / playerModel.timeHeld);
            }

            playerModel.moveDirection.x *= (playerModel.speed / playerModel.timeHeld);
            playerModel.moveDirection.z *= (playerModel.speed / playerModel.timeHeld);
        }
        else
        {
            if (app.view.players[playerID].characterController.isGrounded)
            {
                playerModel.moveDirection.y *= playerModel.speed;
            }

            playerModel.moveDirection.x *= playerModel.speed;
            playerModel.moveDirection.z *= playerModel.speed;
        }



        if (app.view.players[playerID].characterController.isGrounded && playerModel.state.Buttons.A == ButtonState.Pressed)
        {
            playerModel.heldJump += Time.deltaTime;
        }
        else if (app.view.players[playerID].characterController.isGrounded && playerModel.prevState.Buttons.A == ButtonState.Pressed)
        {
            jump();
        }

        if (playerModel.state.Triggers.Right != 0)
        {
            app.view.players[playerID].getAsset().transform.FindChild("Bat").gameObject.GetComponent<BatBehavior>().swingBack();
            playerModel.timeHeld += Time.deltaTime;
        }

        if (playerModel.state.Triggers.Right == 0 && playerModel.timeHeld > 0)
        {
            swing();
            playerModel.timeHeld = 0;
        }

        playerModel.moveDirection.y -= playerModel.gravity * Time.deltaTime;
        app.view.players[playerID].characterController.Move(playerModel.moveDirection * Time.deltaTime);
    }

    void jump()
    {
        PlayerModel playerModel = app.model.players[playerID];

        if (playerModel.heldJump > 0 && playerModel.heldJump < playerModel.jumpThreshold)
        {
            playerModel.moveDirection.y = playerModel.jumpSpeed + (playerModel.heldJump * playerModel.jumpSpeed);
        }
        else
        {
            playerModel.moveDirection.y = playerModel.chargeJump;
        }

        playerModel.heldJump = 0;
    }

    bool isBallInRange()
    {
        return app.view.players[playerID].getAsset().transform.FindChild("HitCube").gameObject.GetComponent<BoxCollider>().bounds.Intersects(app.view.ball.gameObject.GetComponent<SphereCollider>().bounds);
    } 

    void swing()
    {
        if (isBallInRange())
        {
            PlayerModel playerModel = app.model.players[playerID];
            Vector3 camDir = app.view.players[playerID].getAsset().transform.Find("CameraBoom").gameObject.transform.forward;

            camDir.Normalize();

            app.model.players[playerID].movement = camDir * app.model.players[playerID].force * Mathf.Clamp(app.model.players[playerID].timeHeld, 1, playerModel.maxHitChargeTime);

            app.view.ball.gameObject.GetComponent<Rigidbody>().AddForce(app.model.players[playerID].movement);
            app.view.players[playerID].getAsset().GetComponent<AudioSource>().Play();
            GamePad.SetVibration(app.model.players[playerID].mPlayerIndex, 0.25f, 0.25f);
            app.model.startVibe = true;
        }

        app.view.players[playerID].getAsset().transform.FindChild("Bat").gameObject.GetComponent<BatBehavior>().animate();
    }

    void updateVibeTime()
    {
        if(app.model.startVibe)
        {
            app.model.vibeTime += Time.deltaTime;
        }
        if(app.model.vibeTime > 0.25)
        {
            GamePad.SetVibration(app.model.players[playerID].mPlayerIndex, 0.0f, 0.0f);
            app.model.startVibe = false;
            app.model.vibeTime = 0.0f;
        }
    }

    void updateCamera()
    {
        float xDiff =  app.model.players[playerID].state.ThumbSticks.Right.X;
        float yDiff =  app.model.players[playerID].state.ThumbSticks.Right.Y;
        Vector2 mouseDiff = new Vector2(xDiff, yDiff);

        app.view.players[playerID].getAsset().transform.Find("CameraBoom").gameObject.GetComponent<MouseLook>().moveCamera(mouseDiff);
    }

    void Update()
    {
        if (!app.model.startingPhase || !app.model.players[playerID].isForward)
        {
            app.model.players[playerID].prevState = app.model.players[playerID].state;
            app.model.players[playerID].state = GamePad.GetState(app.model.players[playerID].mPlayerIndex);

            updateParticles();
            updateMovement();
            updateCamera();
            updateVibeTime();
        }
    }

    void updateParticles()
    {
        PlayerModel playerModel = app.model.players[playerID];

        //JUMP PARTICLE
        if (!app.view.players[playerID].jumpEmitter.isPlaying && playerModel.heldJump >= playerModel.jumpThreshold)
        {
            app.view.players[playerID].jumpEmitter.Play();
        }

        if (app.view.players[playerID].jumpEmitter.isPlaying)
        {
            if (app.view.players[playerID].characterController.isGrounded)
            {
                if (playerModel.heldJump < playerModel.jumpThreshold)
                {
                    app.view.players[playerID].jumpEmitter.Stop();
                }
            }
            else if (playerModel.moveDirection.y < 0)
            {
                app.view.players[playerID].jumpEmitter.Stop();
            }
        }

        //SWING PARTICLE
        if (!app.view.players[playerID].swingEmitter.isPlaying && playerModel.timeHeld >= playerModel.maxHitChargeTime)
        {
            app.view.players[playerID].swingEmitter.Play();
        }

        if (app.view.players[playerID].swingEmitter.isPlaying && playerModel.timeHeld < playerModel.maxHitChargeTime)
        {
            app.view.players[playerID].swingEmitter.Stop();
        }
    }

    public void reset()
    {
        app.view.players[playerID].getAsset().transform.position = app.model.players[playerID].mStartPosition;
        app.view.players[playerID].getAsset().transform.localRotation = Quaternion.Euler(0, 0, 0);
    }
}