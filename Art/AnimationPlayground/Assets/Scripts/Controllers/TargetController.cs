﻿using UnityEngine;
using System.Collections;

public class TargetController : Element
{
    public TeamKey teamKey;

    void resetShader()
    {
        app.model.goals[teamKey].target.hit = false;
        app.model.goals[teamKey].target.time = 0.0f;
        GetComponent<Renderer>().material.SetFloat("_TIME", 0.0f);
        GetComponent<Renderer>().material.SetFloat("_RADIUS", 0.0f);
    }

    void Update()
    {
        if (app.model.goals[teamKey].target.hit)
        {
            app.model.goals[teamKey].target.time += Time.deltaTime;
            app.view.goals[teamKey].target.GetComponent<Renderer>().material.SetFloat("_TIME", app.model.goals[teamKey].target.time);

            if (app.view.goals[teamKey].target.GetComponent<Renderer>().material.GetFloat("_RADIUS") >= app.view.goals[teamKey].target.GetComponent<Renderer>().material.GetFloat("_END_RADIUS"))
            {
                resetShader();
            }
        }
    }
}
