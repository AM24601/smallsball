﻿using UnityEngine;
using System.Collections;

public class TrackerController : Element
{
    public int playerID;

    void Update()
    {
        app.view.players[playerID].tracker.GetAsset().transform.LookAt(app.view.ball.GetAsset().transform);
    }
}
