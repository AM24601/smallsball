﻿using UnityEngine;
using System.Collections;

public class BallModel : Element 
{
    public Rigidbody rb;
    public float dampening = 0.7f;

    public float force = 500;
    public Vector3 movement = Vector3.zero;

    public Vector3 mInitialPos;

	// Use this for initialization
	void Start () 
    {
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
