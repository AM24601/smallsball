﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Model : Element
{
    public PlayerModel[] players;
    public BallModel ball;
    public Dictionary<TeamKey, TeamModel> teams;
    public Dictionary<TeamKey, GoalModel> goals;
    public bool startingPhase;
    public float beginningTime = 5.0f;
    public float timer;

    public float vibeTime = 0.0f;
    public bool startVibe = false;

    public bool gamePhase;
    public float countdownTime = 180.0f;
    public float countdownTimer;

    // Use this for initialization
    void Start () 
    {
        startingPhase = false;
        teams = new Dictionary<TeamKey, TeamModel>();
        goals = new Dictionary<TeamKey, GoalModel>();

        players = new PlayerModel[app.numPlayers];

        for (int i = 0; i < app.numPlayers; ++i)
        {
            players[i] = gameObject.transform.FindChild("Player" + (i + 1)).GetComponent<PlayerModel>();
        }

        ball = gameObject.transform.FindChild("Ball").GetComponent<BallModel>();

        teams.Add(TeamKey.RED, gameObject.transform.FindChild("redTeamModel").GetComponent<TeamModel>());
        teams.Add(TeamKey.BLUE, gameObject.transform.FindChild("blueTeamModel").GetComponent<TeamModel>());

        goals.Add(TeamKey.RED, gameObject.transform.FindChild("RedGoalModel").GetComponent<GoalModel>());
        goals.Add(TeamKey.BLUE, gameObject.transform.FindChild("BlueGoalModel").GetComponent<GoalModel>());

        timer = beginningTime;
        countdownTimer = countdownTime;
	}
}
