﻿using UnityEngine;
using System.Collections;

public class BallView : Element
{
    void Start()
    {
        gameObject.transform.GetComponent<Rigidbody>().Sleep();
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        app.controller.ball.ReflectBall(collisionInfo);
    }

    public GameObject GetAsset()
    {
        return gameObject;
    }
}
