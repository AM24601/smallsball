﻿using UnityEngine;
using System.Collections;

public class PlayerView : Element
{
    public int playerID;
    public TrackerView tracker;
    public CharacterController characterController;
    public ParticleSystem jumpEmitter;
    public ParticleSystem swingEmitter;

	// Use this for initialization
	void Start ()
    {
        tracker = getAsset().transform.FindChild("BallTracker").GetComponent<TrackerView>();
        characterController = getAsset().GetComponent<CharacterController>();
        jumpEmitter = getAsset().transform.FindChild("jumpParticle").GetComponent<ParticleSystem>();
        swingEmitter = getAsset().transform.FindChild("swingParticle").GetComponent<ParticleSystem>();
	}

    public GameObject getAsset()
    {
        return gameObject.transform.FindChild("asset").gameObject;
    }
}
