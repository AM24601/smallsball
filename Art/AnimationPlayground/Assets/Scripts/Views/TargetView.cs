﻿using UnityEngine;
using System.Collections;

public class TargetView : Element
{
    public TeamKey teamKey;
    public TeamKey goalKey;

    void OnTriggerEnter(Collider other)
    {
        app.controller.goals[teamKey].OnPointScore(other);
        app.controller.goals[goalKey].OnBallEnter();
        app.model.goals[teamKey].target.hit = true;
    }
}
