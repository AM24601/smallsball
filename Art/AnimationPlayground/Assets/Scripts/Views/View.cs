﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class View : Element
{
    public PlayerView[] players;
    public BallView ball;
    public Dictionary<TeamKey, GoalView> goals;
    public GameObject HUD;
   // public BarrierView barrier;

	// Use this for initialization
	void Start ()
    {
        goals = new Dictionary<TeamKey, GoalView>();

        players = new PlayerView[app.numPlayers];

        for (int i = 0; i < app.numPlayers; ++i)
        {
            players[i] = gameObject.transform.FindChild("Player" + (i + 1)).GetComponent<PlayerView>();
            players[i].playerID = i;
        }

        ball = gameObject.transform.FindChild("Ball").GetComponent<BallView>();

        goals.Add(TeamKey.RED, gameObject.transform.FindChild("RedGoalView").GetComponent<GoalView>());
        goals.Add(TeamKey.BLUE, gameObject.transform.FindChild("BlueGoalView").GetComponent<GoalView>());

        goals[TeamKey.RED].SetKey(TeamKey.RED, TeamKey.BLUE);
        goals[TeamKey.BLUE].SetKey(TeamKey.BLUE, TeamKey.RED);

        HUD = gameObject.transform.FindChild("HUD").gameObject;

        //barrier = gameObject.transform.FindChild("Barrier").GetComponent<BarrierView>();
	}
}
