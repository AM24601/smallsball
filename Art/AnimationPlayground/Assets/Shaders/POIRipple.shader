﻿Shader "Hidden/RippleShader"
{
	Properties
	{
		_POI("Impact Point", Vector) = (0.0, 0.0, 0.0)
		_RADIUS("Radius", float) = 0.0
		_RATE("Rate", float) = 2.0
		_TIME("Time", float) = 0.0
		_END_RADIUS("End Radius", float) = 3.0
		_COLOR("Color", Color) = ( 0.0, 0.0, 0.0, 1)
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			float4 _POI;
			float _RATE;
			float _END_RADIUS;
			float _RADIUS;
			float _TIME;
			float4 _COLOR;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 localPos: TEXCOORD1;
			};

			sampler2D _MainTex;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				o.localPos = v.vertex;
				return o;
			}

			half4 frag (v2f i) : SV_Target
			{
				half4 col = tex2D(_MainTex, i.uv);

				float d = distance(_POI, i.localPos);
				_RADIUS = _RATE * _TIME;

				if (_RADIUS > 0 && d <= _RADIUS + 0.05 && d >= _RADIUS - 0.05)
				{
					col = col * _COLOR;
				}

				return col;
			}
			ENDCG
		}
	}
}
